import { en } from './en';
import { el } from './el';
export const LOCALES: {
    [key:string]: any
} = {
    en,
    el
}