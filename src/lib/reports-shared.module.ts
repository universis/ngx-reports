import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MostModule } from '@themost/angular';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { ReportService } from './services/report.service';
import { SharedModule } from '@universis/common';
import { SelectReportComponent } from './components/select-report/select-report.component';
import { FormsModule } from '@angular/forms';
import { FormioModule } from 'angular-formio';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import { ItemDocumentsComponent } from './components/item-documents/item-documents.component';
import {DocumentDownloadComponent} from './components/document-download/document-download.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import { PrintReportComponent } from './components/print-report/print-report.component';
import { DocumentDownloadRoutedComponent } from './components/document-download-routed/document-download-routed.component';
import { NgxSignerModule } from '@universis/ngx-signer';
import { LOCALES } from './i18n';
import { TablesModule } from '@universis/ngx-tables';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FormioModule,
    MostModule,
    SharedModule,
    TranslateModule,
    NgxExtendedPdfViewerModule,
    NgxDropzoneModule,
    NgxSignerModule,
    TablesModule
  ],
  declarations: [
    SelectReportComponent,
    ItemDocumentsComponent,
    DocumentDownloadComponent,
    PrintReportComponent,
    DocumentDownloadRoutedComponent
  ],
  exports: [
    SelectReportComponent,
    ItemDocumentsComponent,
    DocumentDownloadComponent,
    PrintReportComponent,
    DocumentDownloadRoutedComponent
  ],
  entryComponents: [
  ]
})
export class ReportsSharedModule {

    static forRoot(): ModuleWithProviders<ReportsSharedModule> {
        return {
            ngModule: ReportsSharedModule,
            providers: [
              ReportService
            ]
        };
        }
  constructor(private _translateService: TranslateService) {
    Object.keys(LOCALES).forEach( (language: string) => {
      if (Object.prototype.hasOwnProperty.call(LOCALES, language)) {
        this._translateService.setTranslation(language, LOCALES[language], true)
      }
    });
  }

}
